const CooperativaService = require('../services/cooperativa.service')
const httpStatus = require('http-status')

exports.create = (req, res) => {
  CooperativaService.insert(req.body)
    .then(coop => {
      res.status(httpStatus.CREATED)
      res.send(coop)
    })
}

exports.find = (req, res) => {
  CooperativaService.getAll().then(coop => {
    res.status(httpStatus.OK)
    res.send(coop)
  })
}

exports.findById = (req, res) => {
  CooperativaService.getById(req.params.id).then(coop => {
    res.status(httpStatus.OK)
    res.send(coop)
  })
}
exports.updateCoop = (req, res) => {
  CooperativaService.updateCoop(req.params.id, req.body).then(coop => {
    res.status(httpStatus.OK)
    res.send(coop)
  })
}
exports.deleteById = (req, res) => {
  CooperativaService.delete(req.params.id).then(coop => {
    res.status(httpStatus.OK)
    res.send(coop)
  })
}
