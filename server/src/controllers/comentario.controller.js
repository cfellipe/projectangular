const ComentarioService = require('../services/comentario.service')
const httpStatus = require('http-status')

exports.create = (req, res) => {
  ComentarioService.insert(req.body).then(comentario => {
    res.status(httpStatus.CREATED)
    res.send(comentario)
  })
}

exports.find = (req, res) => {
  ComentarioService.getAll(req.params.id).then(coop => {
    res.status(httpStatus.OK)
    res.send(coop)
  })
}
