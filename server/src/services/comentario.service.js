const Comentario = require('../models/comentario.model')

class ComentarioService {
  async insert (comentario) {
    // eslint-disable-next-line no-return-await
    return await new Comentario(comentario).save()
  }

  getAll (id) {
    return new Promise((resolve, reject) => {
      Comentario.find({'idCooperativa': id}).exec((err, docs) => {
        if (err) {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject()
        } else {
          resolve(docs)
        }
      })
    })
  }
}
module.exports = new ComentarioService()
