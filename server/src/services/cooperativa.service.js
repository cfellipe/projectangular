const CooperativaModel = require('../models/cooperativa.model')

class CooperativaService {
  async insert (cooperativa) {
    // eslint-disable-next-line no-return-await
    return await new CooperativaModel(cooperativa).save()
  }

  getAll () {
    return new Promise((resolve, reject) => {
      CooperativaModel.find({}).sort([['siglaCooperativa', 1]]).exec((err, docs) => {
        if (err) {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject()
        } else {
          resolve(docs)
        }
      })
    })
  }
  async getById (id) {
    // eslint-disable-next-line no-return-await
    return await CooperativaModel.findById(id)
  }

  async updateCoop (id, cooperativa) {
    // eslint-disable-next-line no-return-await
    return new Promise((resolve, reject) => {
      CooperativaModel.findByIdAndUpdate(id, cooperativa, {new: true}, (err, coop) => {
        if (err) {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject()
        } else {
          resolve(coop)
        }
      })
    })
  }

  async delete (id) {
    // eslint-disable-next-line no-return-await
    return new Promise((resolve, reject) => {
      CooperativaModel.findByIdAndRemove(id, (err, coop) => {
        if (err) {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject()
        } else {
          resolve(coop)
        }
      })
    })
  }
}
module.exports = new CooperativaService()
