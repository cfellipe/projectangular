const mongoose = require('mongoose')
const Schema = mongoose.Schema

const cooperativaSchema = new Schema({

  nomeCooperativa: {
    type: String
  },
  siglaCooperativa: {
    type: String,
    uppercase: true
  },
  datafundacao: {
    type: Date
  },
  registroOce: {
    type: Number
  },
  registroFencom: {
    type: Number
  },
  nomeGerente: {
    type: String
  },
  emailCooperativa: {
    type: String
  },
  numeroEndCooperativa: {
    type: String
  },
  complemententoCooperativa: {
    type: String
  },
  cidadeCooperativa: {
    type: String
  },
  ufCooperativa: {
    type: String
  },
  telefoneCooperativa: {
    type: String
  },
  site: {
    type: String
  },
  mandato: {
    type: String
  },
  nomePresidente: {
    type: String
  },
  dataNascimentoPresidente: {
    type: Date
  },
  cpfPresidente: {
    type: String
  },
  telefonePresidente: {
    type: String
  },
  enderecoPresidente: {
    type: String
  },
  emailPresidente: {
    type: String
  },
  nomeAdmin: {
    type: String
  },
  nascimentoAdm: {
    type: Date
  },
  cpfAdmin: {
    type: String
  },
  telefoneAdmin: {
    type: String
  },
  enderecoAdmin: {
    type: String
  },
  emailAdmin: {
    type: String
  },
  nomeFinanceiro: {
    type: String
  },
  dataNascimentoFinanceiro: {
    type: Date
  },
  cpfFinanceiro: {
    type: String
  },
  telefoneFinanceiro: {
    type: String
  },
  enderecoFinanceiro: {
    type: String
  },
  emailFinanceiro: {
    type: String
  }

}, {
  timestamps: true
})

module.exports = mongoose.model('Cooperativa', cooperativaSchema)
