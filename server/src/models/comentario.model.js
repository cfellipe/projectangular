const mongoose = require('mongoose')
const Schema = mongoose.Schema

const comentarioSchema = new Schema({

  descricao: {
    type: String
  },
  idCooperativa: {
    type: Schema.Types.ObjectId,
    ref: 'Cooperativa'
  }

}, {
  timestamps: true
})
module.exports = mongoose.model('Comentario', comentarioSchema)
