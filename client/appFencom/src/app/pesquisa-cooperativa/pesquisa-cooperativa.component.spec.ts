import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesquisaCooperativaComponent } from './pesquisa-cooperativa.component';

describe('PesquisaCooperativaComponent', () => {
  let component: PesquisaCooperativaComponent;
  let fixture: ComponentFixture<PesquisaCooperativaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisaCooperativaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisaCooperativaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
