import { Component, OnInit } from '@angular/core';
import { Cooperativa } from './../_models/cooperativa';
import { CooperativaService } from './../_services/cooperativa.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-pesquisa-cooperativa',
  templateUrl: './pesquisa-cooperativa.component.html',
  styleUrls: ['./pesquisa-cooperativa.component.css']
})
export class PesquisaCooperativaComponent implements OnInit {

  public cooperativa: Cooperativa[];

  constructor(private serviceCooperativa: CooperativaService, private spinnerService: Ng4LoadingSpinnerService,
    private confirmationDialogService: ConfirmationDialogService, private toastr: ToastrService) { }

  ngOnInit() {

    this.serviceCooperativa.getCooperativas().then(entidade => {
      console.log(entidade);
      this.cooperativa = entidade;
    });
  }

  public openConfirmationDialog($event, item: Cooperativa): void {
    this.confirmationDialogService.confirm('Excluir', `Deseja excluir a cooperativa ${item.siglaCooperativa} ?`)
      .then((confirmed) => {
        if (confirmed) {
          this.spinnerService.show();

          setTimeout(() => {
            this.spinnerService.hide();
            this.serviceCooperativa.deletePorId(item._id).subscribe(() => {
              this.serviceCooperativa.getCooperativas().then(entidade => {
                this.cooperativa = entidade;
                this.toastr.success(`Removida com sucesso!!`, item.siglaCooperativa);
              });
            }, () => this.toastr.error(`Ocorreu um erro ao excluir `, item.siglaCooperativa)
            );

          }, 2000);
        }
      })
      .catch(() => console.log());
  }

}
