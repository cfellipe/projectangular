import {AbstractModel} from './abstract-model';

export class Cooperativa extends AbstractModel {

    public nomeCooperativa: string;
    public siglaCooperativa: string;
    public dataFundacao: Date;
    public registroOce: number;
    public registroFencom: number;
    public nomeGerente: string;
    public emailCooperativa: string;
    public enderecoCooperativa: string;
    public numeroEndCooperativa: number;
    public complemententoCooperativa: string;
    public cidadeCooperativa: string;
    public ufCooperativa: string;
    public telefoneCooperativa: string;
    public site: string;
    public mandato: string;
    public nomePresidente: string;
    public dataNascimentoPresidente: Date;
    public cpfPresidente: string;
    public telefonePresidente: string;
    public enderecoPresidente: string;
    public emailPresidente: string;
    public nomeAdmin: string;
    public dataNascimentoAdmin: Date;
    public cpfAdmin: string;
    public telefoneAdmin: string;
    public enderecoAdmin: string;
    public emailAdmin: string;
    public nomeFinanceiro: string;
    public dataNascimentoFinanceiro: Date;
    public cpfFinanceiro: string;
    public telefoneFinanceiro: string;
    public enderecoFinanceiro: string;
    public emailFinanceiro: string;


}
