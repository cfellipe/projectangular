export abstract class AbstractModel {

    public _id: string;
    public createdAt: Date;
}
