import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroCooperativaComponent } from './cadastro-cooperativa.component';

describe('CadastroCooperativaComponent', () => {
  let component: CadastroCooperativaComponent;
  let fixture: ComponentFixture<CadastroCooperativaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroCooperativaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroCooperativaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
