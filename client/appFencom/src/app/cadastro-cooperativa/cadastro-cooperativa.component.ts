import { CooperativaService } from './../_services/cooperativa.service';
import { Cooperativa } from './../_models/cooperativa';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastro-cooperativa',
  templateUrl: './cadastro-cooperativa.component.html',
  styleUrls: ['./cadastro-cooperativa.component.css']
})
export class CadastroCooperativaComponent implements OnInit {

  public cooperativa: Cooperativa = new Cooperativa();

  constructor(private serviceCooperativa: CooperativaService, private router: ActivatedRoute,
    private toastr: ToastrService, private route: Router) {
  }

  ngOnInit() {

    if (this.router.snapshot.params['id'] !== undefined) {
      this.serviceCooperativa.getCooperativaById(this.router.snapshot.params['id']).then((coop: Cooperativa) => {
        this.cooperativa = coop;
      });
    }

  }

  public realizaCadastro(): void {
    if (this.cooperativa._id === undefined) {
      this.serviceCooperativa.salvar(this.cooperativa)
        .subscribe((entidade: Cooperativa) => {
          this.toastr.success('Cadastrada com sucesso!!', entidade.siglaCooperativa);
        });
      this.cooperativa = new Cooperativa();
    } else {
      this.serviceCooperativa.editar(this.router.snapshot.params['id'], this.cooperativa)
        .subscribe((entidade: Cooperativa) => {
          this.route.navigate(['/pesquisacoop']);
          this.toastr.success('Editada com sucesso!!', entidade.siglaCooperativa);
        });
      this.cooperativa = new Cooperativa();
    }
}}
