import { environment } from './../../environments/environment';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Cooperativa } from '../_models/cooperativa';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class CooperativaService {

  constructor(private http: Http) { }

  public salvar(cooperativa: Cooperativa): Observable<Cooperativa> {
    const headers: Headers = new Headers();
    headers.append('Content-type', 'application/json');
    return this.http.post(`${environment.api}/cooperativa`,
      JSON.stringify(cooperativa),
      new RequestOptions({ headers: headers })
    )
      .pipe(map((response: Response) => response.json()));

  }

  public getCooperativas(): Promise<Array<Cooperativa>> {
    return this.http.get(`${environment.api}/cooperativa`)
      .toPromise()
      .then(cooperativa => cooperativa.json());
  }

  public getCooperativaById(id: string): Promise<Cooperativa> {
    return this.http.get(`${environment.api}/cooperativa/${id}`)
    .toPromise()
    .then(cooperativa => cooperativa.json());
  }

  public deletePorId(id: string): Observable<Cooperativa> {
    return this.http.delete(`${environment.api}/cooperativa/${id}`).pipe(map((response: Response) => response.json()));
  }

  public editar(id: string, cooperativa: Cooperativa): Observable<Cooperativa> {
    const headers: Headers = new Headers();
    headers.append('Content-type', 'application/json');

    return this.http.put(`${environment.api}/cooperativa/${id}`,
      JSON.stringify(cooperativa),
      new RequestOptions({ headers: headers })
    ).pipe(map((response: Response) => response.json()));

  }
}
