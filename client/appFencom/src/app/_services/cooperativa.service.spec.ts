import { TestBed, inject } from '@angular/core/testing';

import { CooperativaService } from './cooperativa.service';

describe('CooperativaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CooperativaService]
    });
  });

  it('should be created', inject([CooperativaService], (service: CooperativaService) => {
    expect(service).toBeTruthy();
  }));
});
