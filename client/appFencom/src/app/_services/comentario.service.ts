import { Injectable } from '@angular/core';
import { Comentario } from './../_models/comentario';
import { environment } from './../../environments/environment';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ComentarioService {

  constructor(private http: Http) { }

  public salvar(comentario: Comentario): Observable<Comentario> {
      const headers: Headers = new Headers();
      headers.append('Content-type', 'application/json');
      return this.http.post(`${environment.api}/comentario`,
        JSON.stringify(comentario),
        new RequestOptions({ headers: headers })
      ).pipe(map((response: Response) => response.json()));

    }

    public getComentarios(id: string): Promise<Array<Comentario>> {
      return this.http.get(`${environment.api}/comentario/${id}`)
        .toPromise()
        .then(comentario => comentario.json());
    }
}
