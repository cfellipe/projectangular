import { ComentariosCooperativaComponent } from './comentarios-cooperativa/comentarios-cooperativa.component';
import { CadastroCooperativaComponent } from './cadastro-cooperativa/cadastro-cooperativa.component';
import { RouterModule, Routes } from '@angular/router';
import { CooperativasComponent } from './cooperativas/cooperativas.component';
import { PesquisaCooperativaComponent } from './pesquisa-cooperativa/pesquisa-cooperativa.component';

export const routes: Routes = [
    { path: '', component: CooperativasComponent },
    { path: 'cooperativas', component: CooperativasComponent },
    { path: 'cadastrocoop', component: CadastroCooperativaComponent},
    { path: 'cadastrocoop/:id', component: CadastroCooperativaComponent},
    { path: 'pesquisacoop', component: PesquisaCooperativaComponent},
    { path: 'comentarioscoop/:id', component: ComentariosCooperativaComponent}
];

