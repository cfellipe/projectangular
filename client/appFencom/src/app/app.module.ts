import { ComentarioService } from './_services/comentario.service';
import { ConfirmationDialogService } from './confirmation-dialog/confirmation-dialog.service';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { CooperativaService } from './_services/cooperativa.service';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routes } from './app.routes';
import { AppComponent } from './app.component';
import { CabecalhoComponent } from './_components/cabecalho/cabecalho.component';
import { RodapeComponent } from './_components/rodape/rodape.component';
import { CadastroCooperativaComponent } from './cadastro-cooperativa/cadastro-cooperativa.component';
import { CooperativasComponent } from './cooperativas/cooperativas.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { PesquisaCooperativaComponent } from './pesquisa-cooperativa/pesquisa-cooperativa.component';
import { ComentariosCooperativaComponent } from './comentarios-cooperativa/comentarios-cooperativa.component';
import { MomentModule } from 'ngx-moment';


@NgModule({
  declarations: [
    AppComponent,
    CabecalhoComponent,
    RodapeComponent,
    CadastroCooperativaComponent,
    CooperativasComponent,
    PesquisaCooperativaComponent,
    ConfirmationDialogComponent,
    ComentariosCooperativaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MomentModule,
    AngularFontAwesomeModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
      timeOut: 6000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    NgbModule.forRoot(),
    RouterModule.forRoot(routes),
    Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [CooperativaService, ConfirmationDialogService, ComentarioService],
  bootstrap: [AppComponent],
  entryComponents: [ ConfirmationDialogComponent ]
})
export class AppModule { }
