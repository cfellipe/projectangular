import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComentariosCooperativaComponent } from './comentarios-cooperativa.component';

describe('ComentariosCooperativaComponent', () => {
  let component: ComentariosCooperativaComponent;
  let fixture: ComponentFixture<ComentariosCooperativaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComentariosCooperativaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComentariosCooperativaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
