import { ComentarioService } from './../_services/comentario.service';
import { Comentario } from './../_models/comentario';
import { CooperativaService } from './../_services/cooperativa.service';
import { Cooperativa } from './../_models/cooperativa';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


@Component({
  selector: 'app-comentarios-cooperativa',
  templateUrl: './comentarios-cooperativa.component.html',
  styleUrls: ['./comentarios-cooperativa.component.css']
})
export class ComentariosCooperativaComponent implements OnInit {


  public cooperativa: Cooperativa = new Cooperativa();
  public comentario: Comentario = new Comentario();
  public comentarios: Comentario[];

  constructor(private serviceCooperativa: CooperativaService, private router: ActivatedRoute, private serviceComentario: ComentarioService,
    private toastr: ToastrService, private route: Router) { }

  ngOnInit() {

    this.cooperativa._id = this.router.snapshot.params['id'];

    if (this.router.snapshot.params['id'] !== undefined) {
      this.serviceCooperativa.getCooperativaById(this.cooperativa._id).then((coop: Cooperativa) => {
        this.cooperativa = coop;
      });
    }
    this.carragaComentarios();


  }

  public salvaComentario(): void {
    this.comentario.idCooperativa = this.comentario.idCooperativa = this.router.snapshot.params['id'];
    if (this.comentario.descricao !== undefined) {
      this.serviceComentario.salvar(this.comentario).subscribe(() => {
        this.toastr.success('Inserido com sucesso!!', 'Comentario');
        this.carragaComentarios();
      });
      this.comentario.descricao =  null;
    }
  }

  public carragaComentarios(): void {
    this.serviceComentario.getComentarios(this.cooperativa._id).then((comentarios) => {
      this.comentarios = comentarios;
    });
  }

}
